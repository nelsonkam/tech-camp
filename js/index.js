Zepto(function() {
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBHUSzKdMve3lTSjzHHwNyxA7D5mI0chxM",
    authDomain: "tech-camp-b2b2a.firebaseapp.com",
    databaseURL: "https://tech-camp-b2b2a.firebaseio.com",
    projectId: "tech-camp-b2b2a",
    storageBucket: "tech-camp-b2b2a.appspot.com",
    messagingSenderId: "99045541789"
  };
  firebase.initializeApp(config);
  $("#submit-button").on("click", function() {
    var name = $("#name").val();
    var age = $("#age").val();
    var phoneNumber = $("#phone-number").val();
    var email = $("#email").val();
    firebase
      .auth()
      .signInAnonymously()
      .then(function() {
        firebase
          .database()
          .ref("campers")
          .push()
          .set({
            name: name,
            age: age,
            phoneNumber: phoneNumber,
            email: email
          })
          .then(function() {
            $(".registeration").addClass("fade-out");
            // Wait for animation to finish
            setTimeout(function() {
              $(".error").addClass("hidden");
              $(".registeration").addClass("hidden");
              $(".thank-you").removeClass("hidden");
              $(".thank-you").addClass("fade-in");
            }, 1000);
          })
          .catch(function() {
            $(".error").removeClass("hidden");
            $(".error").addClass("fade-in");
          });
      })
      .catch(function(error) {
        $(".error").removeClass("hidden");
        $(".error").addClass("fade-in");
      });
  });
});
